var express = require('express');
var bodyParser = require('body-parser');
var RiveScript = require('rivescript');
var fs = require('fs');


var app = express();
var bots = require("./data.json");
//Initializing first bot
var bot = new RiveScript();
//Initializing brains
const brains = [
  './brains/normal.rive','./brains/normal.rive'
];

function botReady(){
  bot.sortReplies();
  botReply('Hello');
}


function botReply(message){
    var reply = bot.reply("'local-user', message");
    console.log('Bot:' + reply);
}

function botNotReady(err){
  console.log("An error has occurred.", err);
}
//Chargement du cerveau dans le bot
console.log(brains[0]);
bot.loadFile(brains[0]).then(botReady).catch(botNotReady);



app.set('view engine', 'ejs');
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
// parse application/json
app.use(bodyParser.json());
app.use(express.static('public'));


app.get("/", function(req, res) {
    res.render("index");
});


app.post('/admin', function(req, res){
    console.log("Je passe en admin");
    res.render("admin");
});

app.get("/admin", function(req, res) {
    res.render("admin");
});

app.post('/createBot', function (req, res) {
    //res.render("admin");
    var name = req.body.botName;
    var perso = req.body.botPerso;
    
    var botJson = {
        id: bots.length+1,
        name: name,
        url: './brains/'+perso+'.rive',
        personality: perso,
        status: "offline"
    };
    //Et on met à jour le Json et la liste de tâches avec la nouvelle tâche
    bots.push(botJson);
    fs.writeFile('./data.json', JSON.stringify(bots, null, 2), function (err) {
        if (err) return console.log(err);
        console.log("test"+JSON.stringify(bots, null, 2));
        console.log('writing to ' + './data.json');
    });
    res.redirect("/");
});
    


app.use(function(req, res, next){
    res.setHeader('Content-Type', 'text/plain');
    res.send(404, 'Page introuvable !');
});


app.listen(8081);